from datetime import datetime, timedelta

import requests
from rq import use_connection, Queue
from email_helper import send_email


last_year = datetime.now() - timedelta(days=365)
date_formatted = last_year.strftime('%Y%m%d')

KEY = '10287dcdc1488108'
HISTORY_URL = 'http://api.wunderground.com/api/%s/history_%s/q/' % (KEY, date_formatted)
CURRENT_URL = 'http://api.wunderground.com/api/%s/conditions/q/' % KEY

use_connection()
q = Queue()

def send_newsletters():
    # TODO Loop over redis list to queue of the emails
    email = "ryandetzel@gmail.com"
    city = "Boston"
    state = "MA"
    q.enqueue(send_newsletter, email, city, state)


def sends_newsletter(email, city, state):
    """
        Send a newsletter to a single user
    """
    historical_temp = get_historical_temp(city, state)
    current_conditions = get_current_temp_conditions(city, state)

    # TODO Logic to format the subject based off historical/current temp
    subject = "Yours newsletter is here"

    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = from_email
    msg['To'] = to_email

    test = "this is the content..."
    # TODO: Fix content

    part1 = MIMEText(text, 'plain')
    msg.attach(part1)

    send_email(to_email, from_email, msg)


def get_historical_temp(city, state):
    """
        Grab the historical temp from a year ago for a city/state

        Returns a string represention of the tempature
    """
    # TODO: Add cache layer here so we're not hitting the api if we already did
    history_url = "%s/%s/%s.json" % (HISTORY_URL, state, city)
    r = requests.get(history_url)
    json_d = r.json()

    historical_temp = json_d['history']['dailysummary'][0]['meantempi']
    return historical_temp


def get_current_temp_conditions(city, state):
    """
        Grab the current conditions for a certain city and state
        If we already grabbed this in the last 1 hour use that instead

        Returns a tuple, current templates, percentage changed of rain
    """
    # TODO: Add cache layer here so we're not hitting the api if we already did
    current_url = "%s/%s/%s.json" % (CURRENT_URL, state, city)
    r = requests.get(current_url)
    json_d = r.json()

    current_temp = json_d['current_observation']['temp_f']
    precip = json_d['current_observation']['precip_today_in']

    rainy = False
    if float(precip) > 0:
        rainy = True

    return (current_temp, rainy)

if __name__ == "__main__":
    send_newsletters()
