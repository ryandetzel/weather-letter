import uuid
import redis
from time import sleep

from flask import Flask, request, jsonify
from email_helper import send_verify_email

NS_SUBSCRIPTION = "subscription"
NS_VALIDATE = "validate"

app = Flask(__name__)
r = redis.StrictRedis(host='localhost', port=6379, db=0)


@app.route("/")
def index():
    return app.send_static_file('index.html')


@app.route('/subscribe', methods=['POST'])
def subscribe():
    email = request.form['email'].strip()
    city = request.form['city'].strip()
    if '@' in email:
        if not city:
            # should validate the city
            return jsonify(status='error',
                           message="You must pass in a city")

        subscription = r.hexists(NS_SUBSCRIPTION, email)
        if subscription:
            return jsonify(status='error',
                           message="That email address is already signed up")
        else:
            validate_code = uuid.uuid4()
            r.set("%s:%s" % (NS_VALIDATE, validate_code), "%s:%s" % (email, city))
            send_verify_email(email, validate_code)
            return jsonify(status='pending',
                           message="We've sent you an email, please click the link to verify your email address")
        #return flask.jsonify(**f)
    else:
        return jsonify(status='error',
                       message='Invalid email address, please double check you entered it correctly')


@app.route('/verify')
def verify():
    validate_code = request.args.get('code')
    payload = r.get("%s:%s" % (NS_VALIDATE, validate_code))
    (email, location) = payload.split(':')

    if payload:
        r.hset(NS_SUBSCRIPTION, email, location)
        #r.delete("%s:%s" % (NS_VALIDATE, validate_code))
        return "Thanks, you're all set!"
    else:
        return "Error"


@app.route('/unsubscribe')
def unsubscribe():
# TODO: check and remove them from the list
    ass

if __name__ == "__main__":
    app.run()
